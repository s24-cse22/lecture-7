#include <iostream>

using namespace std;


int main(){

    string first_name;
    getline(cin, first_name);

    string last_name;
    getline(cin, last_name);

    string address1;
    getline(cin, address1);

    string address2;
    getline(cin, address2);

    string address3;
    getline(cin, address3);

    string occupation;
    getline(cin, occupation);

    cout << "\\documentclass[12pt]{article}" << endl;
    cout << "\\usepackage[margin=1in]{geometry}" << endl;
    cout << "\\usepackage{amsfonts}" << endl;
    cout << "\\title{Interview Sheet}" << endl;
    cout << "\\author{Angelo Kyrilov}" << endl;
    cout << "\\begin{document}" << endl;
    cout << "\\maketitle" << endl;
    cout << "\\noindent" << endl;
    cout << "\\begin{tabular}{l l}" << endl;
    cout << "Name & " << first_name << " " << last_name << " \\\\\\\\" << endl;
    cout << "Address & " << address1 << "\\\\" << endl;
    cout << "& " << address2 << "\\\\" << endl;
    cout << "& "<< address3 <<"\\\\\\\\" << endl;
    cout << "Occupation & "<< occupation <<"\\\\" << endl;
    cout << "\\end{tabular}" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\hrule" << endl;
    cout << "\\vspace{0.5in}" << endl;
    cout << "\\noindent" << endl;
    cout << "Decision: $\\square$ Yes  $\\square$ No" << endl;
    cout << "\\thispagestyle{empty}" << endl;
    cout << "\\end{document}" << endl;

    return 0;
}